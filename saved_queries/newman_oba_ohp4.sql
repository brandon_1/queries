CREATE OR REPLACE VIEW newman_oba_ohp4 AS 
SELECT
  "date_trunc"('minute', "from_unixtime"(timestamp)) datetime
, "whaleback.man.oba.ohp4.f.al2o3.sensor.pred" oba_al2o3
, "whaleback.man.oba.ohp4.f.fe.sensor.pred" oba_fe
, "whaleback.man.oba.ohp4.f.h2o.sensor.pred" oba_h2o
, "whaleback.man.oba.ohp4.f.p.sensor.pred" oba_p
, "whaleback.man.oba.ohp4.f.sio2.sensor.pred" oba_sio2
, 'NHGF' ore_type
FROM
  ctas_oba_landing
UNION SELECT
  "date_trunc"('minute', "from_unixtime"(timestamp)) datetime
, "whaleback.man.oba.ohp4.l.al2o3.sensor.pred" oba_al2o3
, "whaleback.man.oba.ohp4.l.fe.sensor.pred" oba_fe
, "whaleback.man.oba.ohp4.l.h2o.sensor.pred" oba_h2o
, "whaleback.man.oba.ohp4.l.p.sensor.pred" oba_p
, "whaleback.man.oba.ohp4.l.sio2.sensor.pred" oba_sio2
, 'NHGL' ore_type
FROM
  ctas_oba_landing
