CREATE OR REPLACE VIEW newman_post_crusher_movements AS 
SELECT
  pc.src_id src_id
, pc.src_site_id src_site_id
, pc.src_btch_type_name src_btch_type_name
, (pc.src_start_dt + INTERVAL  '8' HOUR) src_start_dt
, pc.src_prdct_name src_prdct_name
, pc.src_name src_name
, pc.src_source_prdct_name src_source_prdct_name
, pc.src_sc_location_type src_sc_location_type
, (CASE WHEN (pc.dest_prdct_name = 'NHGF') THEN ((pc.src_acc_tonnes * gcm_head.FinesPct) / 100) WHEN (pc.dest_prdct_name = 'NHGL') THEN ((pc.src_acc_tonnes * gcm_head.LumpPct) / 100) END) src_acc_tonnes
, (CASE WHEN (pc.dest_prdct_name = 'NHGF') THEN ((pc.src_est_tonnes * gcm_head.FinesPct) / 100) WHEN (pc.dest_prdct_name = 'NHGL') THEN ((pc.src_est_tonnes * gcm_head.LumpPct) / 100) END) src_est_tonnes
, pc.dest_id dest_id
, pc.dest_btch_type_name dest_btch_type_name
, (pc.dest_start_dt + INTERVAL  '8' HOUR) dest_start_dt
, (pc.dest_end_dt + INTERVAL  '8' HOUR) dest_end_dt
, pc.dest_prdct_name dest_prdct_name
, pc.dest_name dest_name
, pc.dest_sc_location_type dest_sc_location_type
, pc.dest_acc_tonnes dest_acc_tonnes
, pc.dest_est_tonnes dest_est_tonnes
, (gcm.mawdu_modified_date + INTERVAL  '8' HOUR) gcm_last_updated_time
, gcm.moisture gcm_moisture
, gcm.mn gcm_mn
, gcm.mgo gcm_mgo
, gcm.al2o3 gcm_al2o3
, gcm.cao gcm_cao
, gcm.k2o gcm_k2o
, gcm.p gcm_p
, gcm.tio2 gcm_tio2
, gcm.sio2 gcm_sio2
, gcm.s gcm_s
, gcm.fe gcm_fe
, gcm.ultrafines gcm_ultrafines
, (ss551.mawdu_modified_date + INTERVAL  '8' HOUR) ss551_last_updated_time
, ss551.moisture ss551_moisture
, ss551.mn ss551_mn
, ss551.mgo ss551_mgo
, ss551.al2o3 ss551_al2o3
, ss551.cao ss551_cao
, ss551.k2o ss551_k2o
, ss551.p ss551_p
, ss551.tio2 ss551_tio2
, ss551.sio2 ss551_sio2
, ss551.s ss551_s
, ss551.fe ss551_fe
, ss551.ultrafines ss551_ultrafines
, (ps.mawdu_modified_date + INTERVAL  '8' HOUR) ps_last_updated_time
, ps.moisture ps_moisture
, ps.mn ps_mn
, ps.mgo ps_mgo
, ps.al2o3 ps_al2o3
, ps.cao ps_cao
, ps.k2o ps_k2o
, ps.p ps_p
, ps.tio2 ps_tio2
, ps.sio2 ps_sio2
, ps.s ps_s
, ps.fe ps_fe
, ps.ultrafines ps_ultrafines
FROM
  (((((
   SELECT *
   FROM
     newman_mtm_ob18_er_shuttles
UNION    SELECT *
   FROM
     newman_mtm_pc2_pc3
UNION    SELECT *
   FROM
     newman_mtm_pc5
)  pc
INNER JOIN newman_movement_grades gcm ON ((((gcm.movement_id = pc.src_id) AND (gcm.category = 'GRADE CONTROL')) AND (gcm.product_type = pc.dest_prdct_name)) AND (gcm.fe IS NOT NULL)))
LEFT JOIN newman_movement_grades gcm_head ON (((gcm_head.movement_id = pc.src_id) AND (gcm_head.category = 'GRADE CONTROL')) AND (gcm_head.subcategory = 'Head')))
LEFT JOIN newman_movement_grades ps ON ((((ps.movement_id = pc.dest_id) AND (ps.category = 'PRODUCTION SAMPLE')) AND (ps.product_type = pc.dest_prdct_name)) AND (ps.fe IS NOT NULL)))
LEFT JOIN newman_movement_grades ss551 ON ((((ss551.movement_id = pc.src_id) AND (ss551.category = 'PRODUCTION SAMPLE')) AND (ss551.product_type = pc.dest_prdct_name)) AND (ss551.fe IS NOT NULL)))
