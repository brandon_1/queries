CREATE OR REPLACE VIEW newman_mtm_oba_matched AS 
SELECT
  mtm.*
, oba.oba_fe
, oba.oba_sio2
, oba.oba_al2o3
, oba.oba_p
, oba.oba_h2o
FROM
  (newman_post_crusher_movements mtm
LEFT JOIN newman_oba_ohp4 oba ON (((oba.datetime = "date_trunc"('minute', mtm.dest_start_dt)) AND (mtm.dest_prdct_name = oba.ore_type)) AND (mtm.src_name <> 'PC5')))
