CREATE OR REPLACE VIEW newman_movement_grades AS 
SELECT
  (CASE WHEN (subcategory = 'Fines') THEN 'NHGF' WHEN (subcategory = 'Lump') THEN 'NHGL' END) product_type
, movement_id
, "from_unixtime"(mawdu_modified_date) mawdu_modified_date
, category
, subcategory
, "max"((CASE WHEN (name = 'Moisture') THEN value END)) Moisture
, "max"((CASE WHEN (name = 'Mn') THEN value END)) Mn
, "max"((CASE WHEN (name = 'MgO') THEN value END)) Mgo
, "max"((CASE WHEN (name = 'Al2O3') THEN value END)) Al2o3
, "max"((CASE WHEN (name = 'CaO') THEN value END)) Cao
, "max"((CASE WHEN (name = 'K2O') THEN value END)) K2o
, "max"((CASE WHEN (name = 'P') THEN value END)) P
, "max"((CASE WHEN (name = 'TiO2') THEN value END)) Tio2
, "max"((CASE WHEN (name = 'SiO2') THEN value END)) Sio2
, "max"((CASE WHEN (name = 'S') THEN value END)) S
, "max"((CASE WHEN (name = 'Fe') THEN value END)) Fe
, "max"((CASE WHEN (name = 'UltraFines') THEN value END)) UltraFines
, "max"((CASE WHEN (name = 'FinesPct') THEN value END)) FinesPct
, "max"((CASE WHEN (name = 'LumpPct') THEN value END)) LumpPct
FROM
  ctas_api_movements_grades
GROUP BY movement_id, mawdu_modified_date, category, subcategory
