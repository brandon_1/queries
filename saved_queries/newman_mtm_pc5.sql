CREATE OR REPLACE VIEW newman_mtm_pc5 AS 
SELECT
  s2.id src_id
, s2.site src_site_id
, s2.batchtypename src_btch_type_name
, "from_unixtime"(s2.startdate) src_start_dt
, s2.productname src_prdct_name
, s2.sourcename src_name
, s2.sourceproductname src_source_prdct_name
, s2.sourcesupplychainlocationtype src_sc_location_type
, s2.acctonnes src_acc_tonnes
, s2.esttonnes src_est_tonnes
, s1.id dest_id
, s1.batchtypename dest_btch_type_name
, "from_unixtime"(s1.startdate) dest_start_dt
, "from_unixtime"(s1.enddate) dest_end_dt
, s1.productname dest_prdct_name
, s1.destinationname dest_name
, s1.destinationsupplychainlocationtype dest_sc_location_type
, s1.acctonnes dest_acc_tonnes
, s1.esttonnes dest_est_tonnes
FROM
  (ctas_api_movements s1
INNER JOIN ctas_api_movements s2 ON (s2.id = s1.parentmoveid))
WHERE ((((("from_unixtime"(s1.startdate) >= "date_parse"('01-FEB-21', '%d-%b-%y')) AND (s1.site = 'NH')) AND (s1.status = 'CLOSED')) AND (s1.batchtypename = 'MOVE_OHP_STACK')) AND (s2.batchtypename = 'MOVE_PRIMARY_OHP'))
