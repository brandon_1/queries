CREATE OR REPLACE VIEW newman_mtm_pc2_pc3 AS 
SELECT
  s4.id src_id
, s4.site src_site_id
, s4.batchtypename src_btch_type_name
, "from_unixtime"(s4.startdate) src_start_dt
, s4.productname src_prdct_name
, s4.sourcename src_name
, s4.sourceproductname src_source_prdct_name
, s4.sourcesupplychainlocationtype src_sc_location_type
, s4.acctonnes src_acc_tonnes
, s4.esttonnes src_est_tonnes
, s1.id dest_id
, s1.batchtypename dest_btch_type_name
, "from_unixtime"(s1.startdate) dest_start_dt
, "from_unixtime"(s1.enddate) dest_end_dt
, s1.productname dest_prdct_name
, s1.destinationname dest_name
, s1.destinationsupplychainlocationtype dest_sc_location_type
, s1.acctonnes dest_acc_tonnes
, s1.esttonnes dest_est_tonnes
FROM
  (((ctas_api_movements s1
INNER JOIN ctas_api_movements s2 ON (s2.id = s1.parentmoveid))
INNER JOIN ctas_api_movements s3 ON (s3.id = s2.parentmoveid))
INNER JOIN ctas_api_movements s4 ON (s4.id = s3.parentmoveid))
WHERE (((("from_unixtime"(s1.startdate) >= "date_parse"('01-FEB-21', '%d-%b-%y')) AND (s1.site = 'NH')) AND (s1.status = 'CLOSED')) AND (s1.batchtypename = 'MOVE_OHP_STACK'))
